package com.upcodee.assignment_choma.di

import com.upcodee.assignment_choma.db.AppDatabase
import com.upcodee.assignment_choma.db.DatabaseCreator
import com.upcodee.assignment_choma.db.FavoriteDao
import com.upcodee.assignment_choma.db.repositories.FavoriteRepository
import com.upcodee.assignment_choma.db.repositories.FavoriteRepositoryImpl
import com.upcodee.assignment_choma.db.toDao
import com.upcodee.assignment_choma.interactors.GalleryInteractor
import com.upcodee.assignment_choma.interactors.GalleryInteractorImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DbModule {

    @Provides
    @Singleton
    fun provideAppDatabase(databaseCreator: DatabaseCreator): AppDatabase = databaseCreator.create()

    @Provides
    @Singleton
    fun provideFavoriteDao(database: AppDatabase): FavoriteDao = database.favoriteMainDao().toDao()

    @Provides
    @Singleton
    fun provideFavoriteRepository(impl: FavoriteRepositoryImpl): FavoriteRepository = impl
}

@Module
@InstallIn(SingletonComponent::class)
class InteractorModule {

    @Provides
    @Singleton
    fun provideGalleryInteractor(impl: GalleryInteractorImpl): GalleryInteractor = impl
}