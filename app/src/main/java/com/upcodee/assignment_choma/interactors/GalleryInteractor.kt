package com.upcodee.assignment_choma.interactors

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Parcelable
import android.provider.MediaStore
import androidx.exifinterface.media.ExifInterface
import com.upcodee.assignment_choma.db.repositories.FavoriteRepository
import com.upcodee.assignment_choma.domain.Favorite
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import javax.inject.Inject


interface GalleryInteractor {
    fun getFavorites(): Flow<List<Favorite>>
    fun getExifFromUri(uri: Uri): ExifInterface?
    fun getImages(): Flow<List<Image>>
    suspend fun setAsFavorite(id: Int)
    suspend fun removeFavorite(id: Int)
    suspend fun deleteImage(uri: Uri): Int
}

class GalleryInteractorImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val favoritesRepository: FavoriteRepository
) : GalleryInteractor {
    override fun getFavorites(): Flow<List<Favorite>> = favoritesRepository.updates()

    override fun getImages(): Flow<List<Image>> {
        val images = MutableStateFlow<List<Image>>(emptyList())

        val uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

        val projection = arrayOf(
            MediaStore.MediaColumns._ID,
            MediaStore.Images.Media.DISPLAY_NAME,
        )

        val cursor: Cursor? = context.contentResolver.query(uri, projection, null, null, null)

        cursor?.use {
            while (it.moveToNext()) {
                val id: Int = it.getInt(it.getColumnIndexOrThrow(MediaStore.MediaColumns._ID))
                images.value = images.value.plus(
                    Image(
                        assertFileStringUri = Uri.withAppendedPath(uri, id.toString()).toString(),
                        pictureId = id,
                        pictureName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)),
                    )
                )
            }
        }

        return images
    }

    override fun getExifFromUri(uri: Uri): ExifInterface? = context.contentResolver.openInputStream(uri).use {
        it?.let { ExifInterface(it, ExifInterface.STREAM_TYPE_FULL_IMAGE_DATA) }
    }

    override suspend fun setAsFavorite(id: Int) {
        withContext(Dispatchers.IO) {
            favoritesRepository.insert(Favorite(id))
        }
    }

    override suspend fun removeFavorite(id: Int) {
        withContext(Dispatchers.IO) {
            favoritesRepository.delete(id)
        }
    }

    override suspend fun deleteImage(uri: Uri): Int {
        return withContext(Dispatchers.IO) {
            context.contentResolver.delete(uri, null, null)
        }
    }
}

@Parcelize
data class Image(
    val assertFileStringUri: String,
    val pictureId: Int,
    val pictureName: String?,
) : Parcelable