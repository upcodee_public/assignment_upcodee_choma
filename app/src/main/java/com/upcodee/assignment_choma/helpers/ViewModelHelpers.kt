package com.upcodee.assignment_choma.helpers

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavHostController
import kotlinx.coroutines.flow.MutableStateFlow

sealed class Navigate {
    data class Direction(val direction: String) : Navigate()
    object Back : Navigate()
}

class NavigateFlow {
    val navigateFlow = MutableStateFlow<Navigate?>(null)

    fun back() {
        navigateFlow.value = Navigate.Back
    }

    fun direction(navDirections: String) {
        navigateFlow.value = Navigate.Direction(navDirections)
    }
}

interface NavigationViewModel {

    val navigate: NavigateFlow

    fun navigated() {
        navigate.navigateFlow.value = null
    }
}


@Composable
fun NavigationViewModel.observeNavigation(navHostController: NavHostController) {
    val navigation by navigate.navigateFlow.collectAsState(null)
    navigation?.let {
        navigated()
        when (it) {
            is Navigate.Direction -> navHostController.navigateOrPop(it.direction)
            Navigate.Back -> navHostController.popBackStack()
        }
    }
}

fun NavHostController.navigateOrPop(destination: String) {
    if (!popBackStack(destination, false)) {
        navigate(destination)
    }
}