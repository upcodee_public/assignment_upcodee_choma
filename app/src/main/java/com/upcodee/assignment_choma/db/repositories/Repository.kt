package com.upcodee.assignment_choma.db.repositories

import com.upcodee.assignment_choma.db.UniversalDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface Repository<Domain, Key> {
    fun updates(): Flow<List<Domain>>
    suspend fun insert(vararg data: Domain)
    suspend fun delete(id: Key)
}

class RepositoryImpl<DModel, Db, Dao : UniversalDao<Db, Key>, Key>(
    private val dao: Dao,
    val dbToDomain: (Db) -> DModel,
    val domainToDb: (DModel) -> Db,
) : Repository<DModel, Key> {

    private fun Db.toDomain() = dbToDomain(this)
    private fun List<Db>.toDomain() = map { it.toDomain() }
    private fun DModel.toDb() = domainToDb(this)
    private fun List<DModel>.toDb() = map { it.toDb() }

    override suspend fun insert(vararg data: DModel) {
        dao.insertList(data.toList().toDb())
    }

    override suspend fun delete(id: Key) {
        dao.delete(id)
    }

    override fun updates(): Flow<List<DModel>> {
        return dao.updatesAll().map { it?.toDomain() ?: emptyList() }
    }

}