package com.upcodee.assignment_choma.db.repositories

import com.upcodee.assignment_choma.db.FavoriteDao
import com.upcodee.assignment_choma.db.FavoriteDb
import com.upcodee.assignment_choma.domain.Favorite
import javax.inject.Inject

interface FavoriteRepository : Repository<Favorite, Int>

class FavoriteRepositoryImpl @Inject constructor(
    private val dao: FavoriteDao
) : FavoriteRepository, Repository<Favorite, Int> by RepositoryImpl(
    dao,
    { it.convert() },
    { it.convert() }
)

fun FavoriteDb.convert() = Favorite(
    id = id,
)

fun Favorite.convert() = FavoriteDb(
    id = id,
)