package com.upcodee.assignment_choma.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Entity(tableName = "favorites")
data class FavoriteDb(

    @PrimaryKey
    @ColumnInfo(name = "id") val id: Int,
)

@Dao
interface FavoriteMainDao {

    @Query("SELECT * FROM favorites")
    fun updatesAll(): Flow<List<FavoriteDb>?>

    @Query("DELETE FROM favorites WHERE id = :id")
    suspend fun delete(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertList(entity: List<FavoriteDb>)
}

interface FavoriteDao : UniversalDao<FavoriteDb, Int>

interface UniversalDao<Db, Key> {
    fun updatesAll(): Flow<List<Db>?>

    suspend fun delete(id: Key)

    suspend fun insertList(entity: List<Db>)
}

fun FavoriteMainDao.toDao(): FavoriteDao = object : FavoriteDao, FavoriteMainDao by this {}