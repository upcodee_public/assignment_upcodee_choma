package com.upcodee.assignment_choma.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@Database(
    entities = [
        FavoriteDb::class,
    ],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun favoriteMainDao(): FavoriteMainDao
}

class DatabaseCreator @Inject constructor(
    @ApplicationContext private val context: Context
) {

    fun create(): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "app-db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

}