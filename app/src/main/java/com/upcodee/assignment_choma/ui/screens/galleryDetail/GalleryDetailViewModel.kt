package com.upcodee.assignment_choma.ui.screens.galleryDetail

import android.app.RecoverableSecurityException
import android.content.Context
import android.content.IntentSender
import android.os.Build
import android.provider.MediaStore
import androidx.core.net.toUri
import androidx.exifinterface.media.ExifInterface.*
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.upcodee.assignment_choma.domain.Favorite
import com.upcodee.assignment_choma.helpers.NavigateFlow
import com.upcodee.assignment_choma.helpers.NavigationViewModel
import com.upcodee.assignment_choma.helpers.fromJson
import com.upcodee.assignment_choma.interactors.GalleryInteractor
import com.upcodee.assignment_choma.ui.screens.AppNavigations.GALLERY_DETAIL_ARG
import com.upcodee.assignment_choma.ui.screens.gallery.ImageUi
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GalleryDetailViewModel @Inject constructor(
    private val galleryInteractor: GalleryInteractor,
    @ApplicationContext val context: Context,
    savedStateHandle: SavedStateHandle
) : ViewModel(), NavigationViewModel {

    override val navigate: NavigateFlow = NavigateFlow()

    private val _data = MutableStateFlow<ImageDetailUi?>(null)
    private val _favorites = MutableStateFlow<List<Favorite>?>(null)
    private val _args = MutableStateFlow(savedStateHandle.get<String>(GALLERY_DETAIL_ARG)?.fromJson(ImageUi::class.java))

    val data: Flow<ImageDetailUi?> = _data

    init {
        galleryInteractor.getFavorites().combine(_args) { favorites, args ->
            _favorites.value = favorites
            savedStateHandle.get<String>(GALLERY_DETAIL_ARG)?.fromJson(ImageUi::class.java)?.let { imageUi ->
                val exifData = galleryInteractor.getExifFromUri(imageUi.uri.toUri())
                _data.value = ImageDetailUi(
                    uri = imageUi.uri,
                    name = imageUi.name,
                    exifData = exifData?.let {
                        ExifData(
                            height = it.getAttributeInt(TAG_IMAGE_LENGTH, 0),
                            width = it.getAttributeInt(TAG_IMAGE_WIDTH, 0),
                            orientation = when (it.getAttributeInt(TAG_ORIENTATION, 0)) {
                                ORIENTATION_UNDEFINED -> "ORIENTATION_UNDEFINED"
                                ORIENTATION_NORMAL -> "ORIENTATION_NORMAL"
                                ORIENTATION_FLIP_HORIZONTAL -> "ORIENTATION_FLIP_HORIZONTAL"
                                ORIENTATION_ROTATE_180 -> "ORIENTATION_ROTATE_180"
                                ORIENTATION_FLIP_VERTICAL -> "ORIENTATION_FLIP_VERTICAL"
                                ORIENTATION_TRANSPOSE -> "ORIENTATION_TRANSPOSE"
                                ORIENTATION_ROTATE_90 -> "ORIENTATION_ROTATE_90"
                                ORIENTATION_TRANSVERSE -> "ORIENTATION_TRANSVERSE"
                                ORIENTATION_ROTATE_270 -> "ORIENTATION_ROTATE_270"
                                else -> "Unknown"
                            },
                            date = it.getAttribute(TAG_DATETIME),
                        )
                    },
                    isFavorite = favorites.any { it.id == imageUi.original.pictureId }
                )
            }
        }.launchIn(viewModelScope)
    }

    fun onFavoriteClicked() {
        _args.value?.original?.pictureId?.let { imageId ->
            viewModelScope.launch {
                if (_favorites.value?.any { it.id == imageId } == false) {
                    galleryInteractor.setAsFavorite(imageId)
                } else {
                    galleryInteractor.removeFavorite(imageId)
                }
            }
        }
    }

    fun onDeleteClicked(execute: ((IntentSender) -> Unit)? = null) {
        _args.value?.uri?.toUri()?.let {
            viewModelScope.launch {
                try {
                    if (galleryInteractor.deleteImage(it) > 0) {
                        navigate.back()
                    }
                } catch (e: SecurityException) {
                    if (execute == null) {
                        navigate.back()
                    }
                    val intentSender = when {
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                            MediaStore.createDeleteRequest(context.contentResolver, listOf(it)).intentSender
                        }
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                            val recoverableSecurityException = e as? RecoverableSecurityException
                            recoverableSecurityException?.userAction?.actionIntent?.intentSender
                        }
                        else -> null
                    }
                    intentSender?.let { sender ->
                        execute?.invoke(sender)
                    }
                }
            }
        }
    }
}

data class ImageDetailUi(
    val uri: String,
    val name: String,
    val exifData: ExifData?,
    val isFavorite: Boolean,
)

data class ExifData(
    val height: Int,
    val width: Int,
    val orientation: String,
    val date: String?
)