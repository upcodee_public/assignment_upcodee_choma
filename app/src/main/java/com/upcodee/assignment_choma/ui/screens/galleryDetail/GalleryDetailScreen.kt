package com.upcodee.assignment_choma.ui.screens.galleryDetail

import android.app.Activity.RESULT_OK
import android.content.res.Configuration
import android.os.Build
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.ThumbUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import coil.compose.rememberImagePainter
import com.upcodee.assignment_choma.R
import com.upcodee.assignment_choma.helpers.observeNavigation
import com.upcodee.assignment_choma.ui.components.DetailRow
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect

@OptIn(InternalCoroutinesApi::class)
@Composable
fun GalleryDetailScreen(
    navHostController: NavHostController,
    viewModel: GalleryDetailViewModel = hiltViewModel()
) {
    viewModel.observeNavigation(navHostController)
    val configuration = LocalConfiguration.current
    var orientation by remember { mutableStateOf(Configuration.ORIENTATION_PORTRAIT) }

    LaunchedEffect(configuration) {
        snapshotFlow { configuration.orientation }
            .collect { orientation = it }
    }

    val imgData by viewModel.data.collectAsState(null)
    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
        if (it.resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                viewModel.onDeleteClicked()
            }
        }
    }

    imgData?.let {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            TopAppBar(
                title = { Text(modifier = Modifier.padding(end = 20.dp), text = it.name, maxLines = 1, overflow = TextOverflow.Ellipsis) },
                navigationIcon = {
                    IconButton(onClick = { navHostController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = stringResource(R.string.general_back), // v relalnom projekte by som samozrejme vybral vsetky stringy do strings.xml
                        )
                    }
                },
                backgroundColor = Color.Transparent,
                contentColor = MaterialTheme.colors.onBackground,
                elevation = 0.dp,
            )
            when (orientation) {
                Configuration.ORIENTATION_LANDSCAPE -> {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        ImageContent(
                            modifier = Modifier
                                .fillMaxWidth(0.5f)
                                .fillMaxHeight(),
                            data = it,
                            viewModel = viewModel
                        )
                        DataContent(
                            viewModel = viewModel,
                            data = it,
                            launcher = launcher
                        )
                    }
                }
                else -> {
                    ImageContent(
                        modifier = Modifier
                            .fillMaxHeight(0.5f),
                        data = it,
                        viewModel = viewModel
                    )
                    DataContent(
                        viewModel = viewModel,
                        data = it,
                        launcher = launcher
                    )
                }
            }
        }
    }
}

@Composable
private fun DataContent(
    viewModel: GalleryDetailViewModel,
    data: ImageDetailUi,
    launcher: ManagedActivityResultLauncher<IntentSenderRequest, ActivityResult>
) {
    Column {
        Row(
            modifier = Modifier
                .padding(horizontal = 10.dp)
                .fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween
        ) {
            TextButton(onClick = { viewModel.onFavoriteClicked() }) {
                Icon(imageVector = if (data.isFavorite) Icons.Filled.ThumbUp else Icons.Outlined.ThumbUp, contentDescription = "Favorite")
                Spacer(modifier = Modifier.width(5.dp))
                Text(
                    text = if (data.isFavorite) "Unlike" else "Like"
                )
            }
            TextButton(onClick = {
                viewModel.onDeleteClicked {
                    launcher.launch(
                        IntentSenderRequest.Builder(it).build()
                    )
                }
            }) {
                Icon(imageVector = Icons.Outlined.Delete, contentDescription = "Remove")
                Spacer(modifier = Modifier.width(5.dp))
                Text(
                    text = "Remove"
                )
            }
        }
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(vertical = 15.dp)
        ) {
            data.exifData?.let {
                DetailRow(
                    leftText = "Height:",
                    rightText = "${it.height} px"
                )
                DetailRow(
                    leftText = "Width:",
                    rightText = "${it.width} px"
                )
                DetailRow(
                    leftText = "Orientation:",
                    rightText = it.orientation
                )
                it.date?.let {
                    DetailRow(
                        leftText = "Date created:",
                        rightText = it
                    )
                }
            }
        }
    }
}

@Composable
private fun ImageContent(
    modifier: Modifier = Modifier,
    data: ImageDetailUi,
    viewModel: GalleryDetailViewModel
) {
    val shape = MaterialTheme.shapes.medium
    Card(
        modifier = modifier
            .fillMaxWidth()
            .fillMaxHeight(0.5f)
            .padding(start = 10.dp, end = 10.dp, bottom = 10.dp)
            .shadow(5.dp, shape)
            .border(1.dp, MaterialTheme.colors.secondaryVariant, shape),
    ) {
        Box {
            Image(
                painter = rememberImagePainter(data.uri),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxSize()
                    .pointerInput(Unit) {
                        detectTapGestures(
                            onDoubleTap = {
                                viewModel.onFavoriteClicked()
                            }
                        )
                    },
                contentScale = ContentScale.Fit
            )
            Text(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .padding(top = 8.dp)
                    .alpha(0.4f), text = "(Double tap to ${if (data.isFavorite) "unlike" else "like"})",
                color = Color.White
            )
        }
    }
}