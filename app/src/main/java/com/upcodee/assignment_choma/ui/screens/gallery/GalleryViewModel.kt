package com.upcodee.assignment_choma.ui.screens.gallery

import android.os.Parcelable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.upcodee.assignment_choma.helpers.NavigateFlow
import com.upcodee.assignment_choma.helpers.NavigationViewModel
import com.upcodee.assignment_choma.interactors.GalleryInteractor
import com.upcodee.assignment_choma.interactors.Image
import com.upcodee.assignment_choma.ui.screens.AppNavigations
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

@HiltViewModel
class GalleryViewModel @Inject constructor(
    private val galleryInteractor: GalleryInteractor
) : ViewModel(), NavigationViewModel {
    override val navigate: NavigateFlow = NavigateFlow()

    private val _data = MutableStateFlow<List<ImageUi>>(emptyList())
    val data: Flow<List<ImageUi>> = _data

    fun init() {
        galleryInteractor.getFavorites().combine(galleryInteractor.getImages().flowOn(Dispatchers.IO)) { favorites, images ->
            _data.value = images.map { image ->
                ImageUi(
                    image.assertFileStringUri,
                    image.pictureName.orEmpty(),
                    favorites.any { it.id == image.pictureId },
                    image
                )
            }
        }.launchIn(viewModelScope)
    }

    fun onItemClicked(id: Int) {
        _data.value.find { it.original.pictureId == id }?.let {
            navigate.direction(AppNavigations.GALLERY_DETAIL(it))
        }
    }

}

@Parcelize
data class ImageUi(
    val uri: String,
    val name: String,
    val isFavorite: Boolean,
    val original: Image,
) : Parcelable