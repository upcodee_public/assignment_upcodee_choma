package com.upcodee.assignment_choma.ui.screens

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.upcodee.assignment_choma.helpers.toJson
import com.upcodee.assignment_choma.ui.screens.AppNavigations.GALLERY
import com.upcodee.assignment_choma.ui.screens.AppNavigations.GALLERY_DETAIL
import com.upcodee.assignment_choma.ui.screens.AppNavigations.GALLERY_DETAIL_ARG
import com.upcodee.assignment_choma.ui.screens.gallery.GalleryScreen
import com.upcodee.assignment_choma.ui.screens.gallery.ImageUi
import com.upcodee.assignment_choma.ui.screens.galleryDetail.GalleryDetailScreen
import com.upcodee.assignment_choma.ui.theme.MyTheme

@Composable
fun App() {
    MyTheme {
        Surface(color = MaterialTheme.colors.background) {
            AppNavigation()
        }
    }
}

@Composable
fun AppNavigation() {
    val controller = rememberNavController()
    NavHost(navController = controller, startDestination = GALLERY) {
        composable(GALLERY) { GalleryScreen(controller) }
        composable(GALLERY_DETAIL, arguments = listOf(navArgument(GALLERY_DETAIL_ARG) { type = NavType.StringType })) { GalleryDetailScreen(controller) }
    }
}

object AppNavigations {
    const val GALLERY = "GALLERY"
    fun GALLERY_DETAIL(image: ImageUi) = "${GALLERY_DETAIL_TAG}?$GALLERY_DETAIL_ARG=${image.toJson()}"
    const val GALLERY_DETAIL_TAG = "GALLERY_DETAIL"
    const val GALLERY_DETAIL_ARG = "upgrade_detail_level"
    const val GALLERY_DETAIL = "{$GALLERY_DETAIL_TAG}?$GALLERY_DETAIL_ARG={$GALLERY_DETAIL_ARG}"
}