package com.upcodee.assignment_choma.ui.screens.gallery

import android.os.Build
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import coil.compose.rememberImagePainter
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.upcodee.assignment_choma.helpers.observeNavigation
import com.upcodee.assignment_choma.ui.components.LikeComponent

@OptIn(ExperimentalPermissionsApi::class, ExperimentalFoundationApi::class)
@Composable
fun GalleryScreen(
    navHostController: NavHostController,
    viewModel: GalleryViewModel = hiltViewModel()
) {
    viewModel.observeNavigation(navHostController)

    val readWriteExternalStoragePermissionState = rememberMultiplePermissionsState(
        listOfNotNull(
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) android.Manifest.permission.WRITE_EXTERNAL_STORAGE else null
        )
    )

    val imgData by viewModel.data.collectAsState(emptyList())

    val allPermissionsGranted = readWriteExternalStoragePermissionState.allPermissionsGranted

    LaunchedEffect(key1 = allPermissionsGranted, block = {
        if (allPermissionsGranted) {
            viewModel.init()
        }
    })

    Scaffold(modifier = Modifier.fillMaxSize()) {
        if (!allPermissionsGranted) {
            Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
                Text(text = "You need to accept permissions to access gallery.")
                Spacer(modifier = Modifier.height(10.dp))
                Button(onClick = { readWriteExternalStoragePermissionState.launchMultiplePermissionRequest() }) {
                    Text("Request permissions")
                }
            }
        } else {
            if (imgData.isEmpty()) {
                Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
                    Text(text = "There are no images in your device.")
                }
            }
            LazyVerticalGrid(
                cells = GridCells.Adaptive(100.dp),
                contentPadding = PaddingValues(0.dp)
            ) {
                items(imgData) { item ->
                    Box(
                        modifier = Modifier
                            .size(130.dp)
                            .padding(1.dp)
                            .border(1.dp, MaterialTheme.colors.secondaryVariant)
                    ) {
                        Image(
                            painter = rememberImagePainter(item.uri),
                            contentDescription = null,
                            modifier = Modifier
                                .fillMaxSize()
                                .clickable {
                                    viewModel.onItemClicked(item.original.pictureId)
                                },
                            contentScale = ContentScale.Crop
                        )
                        LikeComponent(item.isFavorite)
                    }
                }
            }
        }
    }
}