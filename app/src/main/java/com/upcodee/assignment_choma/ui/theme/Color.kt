package com.upcodee.assignment_choma.ui.theme

import androidx.compose.ui.graphics.Color

val Red200 = Color(0xFFFC8686)
val Red500 = Color(0xFFEE0000)
val Red700 = Color(0xFFB30000)
val Teal200 = Color(0xFF03DAC5)
val Gray200 = Color(0xFF353535)
val Gray500 = Color(0xFF252525)